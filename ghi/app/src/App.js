import Nav from './Nav';
import AttendeesList from './AttendeesList';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm'
import PresentationForm from './PresentationForm'
import AttendConference from'./AttendConferenceForm'
import AttendConferenceForm from './AttendConferenceForm';

function App(props) {
  return (
    <div>
      Number of attendees: {props.attendees.length}
    </div>
  );
}

export default App;

  if (props.attendees === undefined){
    return null;
  }
  return (
  <BrowserRouter>
    <Nav />
    <div className ="container">
      <Routes>
      <Route path = "attendees" element={<AttendeesList/>} />
          <Route path = "new" element = {<AttendConferenceForm/>} />
        <Route path = "locations">
          <Route path = "new" element={<LocationForm />} />
        </Route>
        <Route path = "conferences">
          <Route path = "new" element = {<ConferenceForm/>} />
        </Route>
        <Route path = "presentations">
          <Route path = "new" element = {<PresentationForm />} />
        </Route>
      </Routes>
    </div>
  </BrowserRouter>
  );
}

export default App;
